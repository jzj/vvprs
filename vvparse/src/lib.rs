/*
 * File:    TODO.rs
 * Brief:   TODO
 *
 * Copyright (C) TODO John Jekel
 * See the LICENSE file at the root of the project for licensing info.
 *
 * TODO longer description
 *
*/

/*!
 * TODO rustdoc for this file here
*/

/* ------------------------------------------------------------------------------------------------
 * Submodules
 * --------------------------------------------------------------------------------------------- */

//TODO (includes "mod ..." and "pub mod ...")

/* ------------------------------------------------------------------------------------------------
 * Uses
 * --------------------------------------------------------------------------------------------- */

use std::io::BufRead;

/* ------------------------------------------------------------------------------------------------
 * Macros
 * --------------------------------------------------------------------------------------------- */

//TODO (also pub(crate) use the_macro statements here too)

/* ------------------------------------------------------------------------------------------------
 * Constants
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Static Variables
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Types
 * --------------------------------------------------------------------------------------------- */

#[derive(Debug)]
pub struct ThreadInstruction {
    //TODO
}

#[derive(Debug)]
pub struct VerilogThread {
    instructions: Vec<ThreadInstruction>
}

#[derive(Debug)]
pub struct VVPSyntaxTree {
    pub ivl_version:            Option<String>,
    pub ivl_delay_selection:    Option<String>,
    pub vpi_time_precision:     Option<(bool, u128)>,
    pub vpi_modules:            Vec<String>,
    pub verilog_threads:        Vec<VerilogThread>
    //TODO more 
}

struct LineTokens<'a> {
    char_iter: Option<std::str::Chars<'a>>
}

enum ParseState {
    InRoot,
    InScope,
    InThread,
    InFileList
}

/* ------------------------------------------------------------------------------------------------
 * Associated Functions and Methods
 * --------------------------------------------------------------------------------------------- */

    /*
impl LineTokens {
    fn new(line: &str) -> LineTokens {
        LineTokens {
            char_iter: line.chars()
        }
    }

    fn iter(&mut self) -> &str {
        todo!()
    }
}
    */

/* ------------------------------------------------------------------------------------------------
 * Traits And Default Implementations
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Trait Implementations
 * --------------------------------------------------------------------------------------------- */

impl<'a> Iterator for LineTokens<'a> {
    type Item = String;

    fn next(self: &mut LineTokens::<'a>) -> Option<String> {
        let chars = self.char_iter.as_mut()?;

        let mut first_token_char = ' ';//So we bail out if the line's completely empty
        
        for c in &mut *chars {
            first_token_char = c;

            //Encountered comment
            if first_token_char == '#' {
                self.char_iter = None;
                return None;
            }

            //Trim leading whitespace
            if !first_token_char.is_whitespace() {
                break;
            }
        }
        if first_token_char.is_whitespace() || (first_token_char == ';') {//Out of tokens on this line
            return None;
        }

        let mut token = String::with_capacity(120);
        token.push(first_token_char);

        let mut in_string = first_token_char == '"';
        for c in chars {
            if c.is_whitespace() && !in_string {
                break;
            }

            if c == ';' && !in_string {//End of line
                self.char_iter = None;
                break;
            }

            token.push(c);

            if c == '"' && in_string {
                break;
            }
        }

        Some(token)
    }
}

/* ------------------------------------------------------------------------------------------------
 * Functions
 * --------------------------------------------------------------------------------------------- */

pub fn parse(input: &mut impl BufRead) -> Result<VVPSyntaxTree, std::io::Error> {
    let mut tree = VVPSyntaxTree {
        ivl_version:            None,
        ivl_delay_selection:    None,
        vpi_time_precision:     None,
        vpi_modules:            Vec::new(),
        verilog_threads:        Vec::new()
    };

    //TODO do we need multi-line support?
    let mut state = ParseState::InRoot;
    for line in input.lines() {
        let line = line?;
        let mut tokens = tokenize(&line);
        match state {
            ParseState::InRoot => {
                parse_root_line(&mut state, &mut tree, &mut tokens);
            },
            ParseState::InScope => {
                todo!();//Scope not implemented yet
            },
            ParseState::InThread => {
                parse_thread_line(&mut state, &mut tree, &mut tokens);
            },
            ParseState::InFileList => {
                todo!();//File list not implemented yet
            }
        }
    }

    Ok(tree)
}

fn tokenize(line: &str) -> LineTokens {
    LineTokens {
        char_iter: Some(line.chars())
    }
}

fn parse_root_line(state: &mut ParseState, tree: &mut VVPSyntaxTree, tokens: &mut LineTokens) {
    //Parse the line's tokens
    if let Some(first_token) = tokens.next() {
        let first_char = first_token.chars().next().unwrap();
        dbg!(&first_char);
        match first_char {
            ':' => {
                match first_token.as_str() {
                    ":ivl_version" => {
                        let ivl_version = tokens.next().unwrap();
                        //TODO ensure this is actually a string
                        let ivl_version = &ivl_version[1..ivl_version.len()-1];
                        //println!("IVL Version: {}", ivl_version);
                        tree.ivl_version = Some(ivl_version.to_string());
                    },
                    ":ivl_delay_selection" => {
                        let ivl_delay_selection = tokens.next().unwrap();
                        //TODO ensure this is actually a string
                        let ivl_delay_selection = &ivl_delay_selection[1..ivl_delay_selection.len()-1];
                        //println!("IVL Delay Selection: {}", ivl_delay_selection);
                        tree.ivl_delay_selection = Some(ivl_delay_selection.to_string());
                    },
                    ":vpi_time_precision" => {
                        let positive    = tokens.next().unwrap() == "+";
                        let power: u128 = tokens.next().unwrap().parse().unwrap();
                        //println!("VPI Time Precision: 10E{}{}", if positive {'+'} else {'-'}, power);
                        tree.vpi_time_precision = Some((positive, power));
                    },
                    ":vpi_module" => {
                        let vpi_filename = tokens.next().unwrap();
                        //TODO ensure this is actually a string
                        let vpi_filename = &vpi_filename[1..vpi_filename.len()-1];
                        //println!("VPI Module: {}", vpi_filename);
                        tree.vpi_modules.push(vpi_filename.to_string());
                    },
                    //TODO
                    _ => {
                        todo!();//Directive not implemented yet
                    }
                };
            }
            'S' => {
                println!("Scope TODO");
                //*state = ParseState::InScope;
                //todo!();//Scope not implemented yet
            },
            'T' => {
                *state = ParseState::InThread;
                tree.verilog_threads.push(VerilogThread { instructions: Vec::new() });
                //todo!();//Thread not implemented yet
            },
            _ => {//Verilog code
                println!("TODO handle \"{}\"", first_token);
                //todo!();//Line type not yet implemented
            }
        }
    }
}

fn parse_thread_line(state: &mut ParseState, tree: &mut VVPSyntaxTree, tokens: &mut LineTokens) {
    //Parse the line's tokens
    if let Some(first_token) = tokens.next() {
        let first_char = first_token.chars().next().unwrap();
        dbg!(&first_char);
        match first_char {
            '%' => {
                match first_token.as_str() {
                    "%pushi/vec4" => {
                        todo!();
                    },
                    _ => {
                        todo!();
                    }
                };
            },
            '.' => {
                //TODO additional sanity checks (end of thread)
                *state = ParseState::InRoot;
            },
            _ => {
                todo!();
            }
        }
    }
}

/* ------------------------------------------------------------------------------------------------
 * Tests
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Benchmarks
 * --------------------------------------------------------------------------------------------- */

//TODO
