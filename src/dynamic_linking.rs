/*
 * File:    dynamic_linking.rs
 * Brief:   TODO
 *
 * Copyright (C) 2024 John Jekel
 * See the LICENSE file at the root of the project for licensing info.
 *
 * TODO longer description
 *
*/

/*!
 * TODO rustdoc for this file here
*/

/* ------------------------------------------------------------------------------------------------
 * Uses
 * --------------------------------------------------------------------------------------------- */

use std::ffi::{OsStr};
use std::sync::{Mutex, OnceLock};

//Note: We have to use Unix-specific libloading variants so that we can specify RTLD_GLOBAL
//for exposing some symbols back to the VPI/PLI/DPI libraries we load
use libloading::os::unix::{Library as UnixLibrary, Symbol as UnixSymbol, RTLD_NOW, RTLD_GLOBAL};

//For everything else use the regular variants since they're safer
use libloading::{Library, Symbol};

/* ------------------------------------------------------------------------------------------------
 * Constants
 * --------------------------------------------------------------------------------------------- */

const OUR_UNIX_LIB_STR: &str = "libvvprs.so";

const EXTRA_UNIX_LIB_STRS: [&str; 1] = [
    //Since -rdynamic isn't working, we need to build a dynamic library and then
    //in addition to the executable statically linking to it, it will also dynamically
    //link to it as well to get the necessary C symbols for other libraries it loads
    //But there's a gotcha! This won't actually work, since then we have two copies of the
    //simulator state! So instead we expect that the executable has already dynamically
    //loaded the `cdylib` version of this library with the RTLD_GLOBAL flag
    //"libvvprs.so",

    //Libraries assume a fully functional C environment, and thus need libm to exist
    "libm.so.6"
];

const TOTAL_UNIX_LIBS: usize = 1 + EXTRA_UNIX_LIB_STRS.len();

/* ------------------------------------------------------------------------------------------------
 * Static Variables
 * --------------------------------------------------------------------------------------------- */

//Needed since dynamic library loading in inherently a global operation

//A mutex is fine this this is only ever really written to
static other_libs: Mutex<Vec<Library>> = Mutex::new(Vec::new());

//OnceLock works since this is only ever written to once
//TODO perhaps one lock for the whole array?
static unix_libs:  [OnceLock<UnixLibrary>; TOTAL_UNIX_LIBS] = [const { OnceLock::new() }; TOTAL_UNIX_LIBS];

/* ------------------------------------------------------------------------------------------------
 * Functions
 * --------------------------------------------------------------------------------------------- */

//Will load the cdylib version of this library, other libraries that VPI/PLI/DPI modules
//may need global symbols from, and then call that version's actual_simulator_main
pub fn simulator_main() {
    //SAFETY: We're reloading ourselves and calling the cdylib version of `finish_init_symbol`
    //so we know we don't have nastiness in our init or deinit routines
    let our_lib = unsafe {
        UnixLibrary::open(Some(OUR_UNIX_LIB_STR), RTLD_NOW | RTLD_GLOBAL)
    }.expect("Failed to load self!");

    //SAFETY: The loaded version of ourselves has a valid version of this function too
    let finish_global_symbol_init_symbol: UnixSymbol<extern "C" fn(UnixLibrary)> = unsafe {
        our_lib.get(b"finish_global_symbol_init")
    }.expect("Failed to get finish_global_symbol_init symbol!");

    //SAFETY: It should be safe to call the wrapper function
    unsafe { finish_global_symbol_init_symbol(our_lib); }
}

#[no_mangle]
pub extern "C" fn finish_global_symbol_init(our_lib: UnixLibrary) {
    unix_libs[0].set(our_lib).expect("finish_global_symbol_init called more than once!");
    
    for (ii, unix_lib_str) in EXTRA_UNIX_LIB_STRS.iter().enumerate() {
        //SAFETY: The libraries we're loading are assumed to not have nastiness in
        //their init or deinit routines
        let unix_lib = unsafe {
            UnixLibrary::open(Some(unix_lib_str), RTLD_NOW | RTLD_GLOBAL)
        }.expect("Failed to load required library!");
        
        unix_libs[ii+1].set(unix_lib).expect("init() called more than once!");
    }

    crate::main::actual_simulator_main();

    //No need for a deinit function since the OS will clean up the libraries when the process exits
}

pub fn load(filename: impl AsRef<OsStr>) -> Result<(), ()> {//TODO better error type
    //SAFETY: The libraries we're loading are assumed to not have nastiness in
    //their init or deinit routines
    let lib = unsafe {
        Library::new(filename)
    }.map_err(|_| ())?;

    //SAFETY: It's assumed that, if the library provides a vlog_startup_routines array,
    //it is a valid NULL-terminated array of function pointers taking and returning nothing
    let vlog_startup_routines: Symbol<*const unsafe extern "C" fn()> = unsafe {
        lib.get(b"vlog_startup_routines")
    }.map_err(|_| ())?;
    dbg!(&vlog_startup_routines);

    //Since we can't really expresses a variable sized NULL terminated array as a Rust type,
    //we need to get the raw pointer
    //SAFETY: We still have the library loaded, and will eventually add it to `other_libs` and
    //keep it loaded forever, so the symbol will always be valid
    let vlog_startup_routines: *const unsafe extern "C" fn() = unsafe {
        vlog_startup_routines.into_raw()
    }.into_raw().cast();

    //SAFETY: It's assumed that, if the library provides a vlog_startup_routines array,
    //it is a valid NULL-terminated array of function pointers taking and returning nothing
    //Also the startup routine itself shouldn't do anything nasty
    let mut current_routine_ptr = vlog_startup_routines;
    loop {
        let check_if_null_ptr: *const *const u8 = current_routine_ptr.cast();
        let check_if_null = unsafe { *check_if_null_ptr };//Should be fine since the array is valid
        if check_if_null.is_null() {//End of the array
            break;
        }
        unsafe { (*current_routine_ptr)(); }

        //SAFETY: The array should be valid, so there should be another element or NULL after this
        unsafe { current_routine_ptr = current_routine_ptr.add(1); }
    }
    
    //Add the library to the list of other libraries so it never gets unloaded
    other_libs.lock().expect("lock shouldn't be poisioned").push(lib);

    Ok(())

    //No need for a deinit function since the OS will clean up the libraries when the process exits
}
