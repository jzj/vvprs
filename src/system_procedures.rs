/*
 * File:    system_procedures.rs
 * Brief:   TODO
 *
 * Copyright (C) TODO John Jekel
 * See the LICENSE file at the root of the project for licensing info.
 *
 * TODO longer description
 *
*/

/*!
 * TODO rustdoc for this file here
*/

/* ------------------------------------------------------------------------------------------------
 * Submodules
 * --------------------------------------------------------------------------------------------- */

//TODO (includes "mod ..." and "pub mod ...")

/* ------------------------------------------------------------------------------------------------
 * Uses
 * --------------------------------------------------------------------------------------------- */

use std::collections::HashMap;
use std::sync::RwLock;

/* ------------------------------------------------------------------------------------------------
 * Macros
 * --------------------------------------------------------------------------------------------- */

//TODO (also pub(crate) use the_macro statements here too)

/* ------------------------------------------------------------------------------------------------
 * Constants
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Types
 * --------------------------------------------------------------------------------------------- */

pub type Callback = Option<unsafe extern "C" fn(*mut sv_bindings::PLI_BYTE8) -> sv_bindings::PLI_INT32>;

#[derive(Clone)]
pub enum FunctionType {
    Integer,
    RealFunction,

}

#[derive(Clone)]
pub enum ProdedureType {
    Task,
    Function(FunctionType)
}

#[derive(Clone)]
pub struct ProcedureInfo {
    //No need for the name since that's stored in the HashMap
    pub procedure_type:     ProdedureType,
    pub call_callback:      Callback,
    pub compile_callback:   Callback,
    pub size_callback:      Callback,
    pub user_data:          *mut sv_bindings::PLI_BYTE8
}

/* ------------------------------------------------------------------------------------------------
 * Static Variables
 * --------------------------------------------------------------------------------------------- */

static registered_system_procedures: RwLock<Option<HashMap<String, ProcedureInfo>>> = RwLock::new(None);

/* ------------------------------------------------------------------------------------------------
 * Associated Functions and Methods
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Traits And Default Implementations
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Trait Implementations
 * --------------------------------------------------------------------------------------------- */

//TODO

//TODO ensure this is safe
unsafe impl Sync for ProcedureInfo {}
unsafe impl Send for ProcedureInfo {}

/* ------------------------------------------------------------------------------------------------
 * Functions
 * --------------------------------------------------------------------------------------------- */

pub fn init() {
    registered_system_procedures.write()
        .expect("lock shouldn't be poisoned")
        .replace(HashMap::new());
}

//TODO is it okay to just return the old version?
pub fn register_procedure(name: &str, procedure_info: ProcedureInfo) -> Option<ProcedureInfo> {
    registered_system_procedures.write()
        .expect("lock shouldn't be poisoned")
        .as_mut()
        .expect("hash map should be initialized")
        .insert(name.to_string(), procedure_info)
}

pub fn get_info(name: &str) -> Option<ProcedureInfo> {
    Some(registered_system_procedures.read()
        .expect("lock shouldn't be poisoned")
        .as_ref()
        .expect("hash map should be initialized")
        .get(name)?.clone())
}

/* ------------------------------------------------------------------------------------------------
 * Tests
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Benchmarks
 * --------------------------------------------------------------------------------------------- */

//TODO
