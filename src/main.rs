/*
 * File:    main.rs
 * Brief:   TODO
 *
 * Copyright (C) TODO John Jekel
 * See the LICENSE file at the root of the project for licensing info.
 *
 * TODO longer description
 *
*/

/*!
 * TODO rustdoc for this file here
*/

/* ------------------------------------------------------------------------------------------------
 * Submodules
 * --------------------------------------------------------------------------------------------- */

//TODO (includes "mod ..." and "pub mod ...")

/* ------------------------------------------------------------------------------------------------
 * Uses
 * --------------------------------------------------------------------------------------------- */

use vvparse::parse as vvparse;

/* ------------------------------------------------------------------------------------------------
 * Macros
 * --------------------------------------------------------------------------------------------- */

//TODO (also pub(crate) use the_macro statements here too)

/* ------------------------------------------------------------------------------------------------
 * Constants
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Static Variables
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Types
 * --------------------------------------------------------------------------------------------- */

//TODO includes "type"-defs, structs, enums, unions, etc

/* ------------------------------------------------------------------------------------------------
 * Associated Functions and Methods
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Traits And Default Implementations
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Trait Implementations
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Functions
 * --------------------------------------------------------------------------------------------- */

pub fn actual_simulator_main() {
    let parse_thread_handle = std::thread::spawn(|| {
        let args: Vec<String> = std::env::args().collect();
        assert!(args.len() == 2);
        let file                = std::fs::File::open(&args[1]).unwrap();
        let mut buffered_file   = std::io::BufReader::new(file);
        vvparse(&mut buffered_file)
    });

    //Initialization must happen on the main thread, so we continue on with this
    //while concurrently parsing files
    crate::system_procedures::init();
    //TODO other init

    let parse_result = parse_thread_handle.join().unwrap().unwrap();
    dbg!(&parse_result);
    
    //Load VPI modules
    for module in parse_result.vpi_modules.iter() {
        //println!("Loading VPI module: {}", module);
        crate::dynamic_linking::load(&module).unwrap();
    }

    println!("Made it!");
}

/* ------------------------------------------------------------------------------------------------
 * Tests
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Benchmarks
 * --------------------------------------------------------------------------------------------- */

//TODO
