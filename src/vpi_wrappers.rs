/*
 * File:    vpi_wrappers.rs
 * Brief:   TODO
 *
 * Copyright (C) TODO John Jekel
 * See the LICENSE file at the root of the project for licensing info.
 *
 * TODO longer description
 *
*/

/*!
 * TODO rustdoc for this file here
*/

/* ------------------------------------------------------------------------------------------------
 * Submodules
 * --------------------------------------------------------------------------------------------- */

//TODO (includes "mod ..." and "pub mod ...")

/* ------------------------------------------------------------------------------------------------
 * Uses
 * --------------------------------------------------------------------------------------------- */

use sv_bindings::*;

/* ------------------------------------------------------------------------------------------------
 * Macros
 * --------------------------------------------------------------------------------------------- */

macro_rules! stub {
    ($name:ident) => {
        #[no_mangle]
        pub extern "C" fn $name() {
            println!("Stub function {} called", stringify!($name));
            todo!()
        }
    };
}

/* ------------------------------------------------------------------------------------------------
 * Constants
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Static Variables
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Types
 * --------------------------------------------------------------------------------------------- */

//TODO includes "type"-defs, structs, enums, unions, etc

/* ------------------------------------------------------------------------------------------------
 * Associated Functions and Methods
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Traits And Default Implementations
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Trait Implementations
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Functions
 * --------------------------------------------------------------------------------------------- */

stub!(vpi_put_delays);
stub!(vpi_free_object);
stub!(vpi_mcd_flush);
stub!(vpi_handle_by_index);
stub!(vpi_mcd_close);
stub!(vpi_put_value);
stub!(vpi_scan);
stub!(vpi_control);
stub!(vpi_get_userdata);
stub!(vpi_get_file);
stub!(vpi_get_value);
stub!(vpi_get_str);
stub!(vpi_remove_cb);
stub!(vpi_put_userdata);
stub!(vpi_get_delays);
stub!(vpi_mcd_printf);
stub!(vpi_fopen);
stub!(vpi_printf);
stub!(vpi_handle_by_name);
stub!(vpi_mcd_vprintf);
stub!(vpi_iterate);
stub!(vpi_get_time);
stub!(vpi_mcd_open);
stub!(vpi_get);

stub!(vpip_format_strength);
stub!(vpip_mcd_rawwrite);
stub!(vpip_set_return_value);
stub!(vpip_count_drivers);
stub!(vpip_calc_clog2);

#[no_mangle]
pub extern "C" fn vpi_register_systf(systf_data_ptr: *mut t_vpi_systf_data) -> vpiHandle {//FIXME proper return type and argument
    println!("Stub function {} called", "vpi_register_systf");
    //TESTING
    let systf_data = unsafe { *systf_data_ptr };
    dbg!(&systf_data);
    let tfname = systf_data.tfname;
    let tfname = unsafe { std::ffi::CStr::from_ptr(tfname) };
    dbg!(tfname);

    crate::system_procedures::register_procedure(
        tfname.to_str().unwrap(),
        crate::system_procedures::ProcedureInfo {
            procedure_type:     crate::system_procedures::ProdedureType::Task,//TODO
            call_callback:      systf_data.calltf,
            compile_callback:   systf_data.compiletf,
            size_callback:      systf_data.sizetf,
            user_data:          systf_data.user_data
        }
    );

    let vpi_handle = crate::vpi_handle::VpiHandle::SystemProcedure(tfname.to_str().unwrap().to_string());
    vpi_handle.into_boxed_ffi_pointer()
}

#[no_mangle]
pub extern "C" fn vpi_get_vlog_info(vlog_info_ptr: *mut t_vpi_vlog_info) {
    //TODO justify safety
    let vlog_info = unsafe {
        &mut *vlog_info_ptr
    };

    vlog_info.argc    = 0;//TODO
    vlog_info.argv    = std::ptr::null_mut();//TODO
    vlog_info.product = const { c"VVPRS".as_ptr() as *mut i8 };
    vlog_info.version = const { c"0.0.0".as_ptr() as *mut i8 };
    
    println!("Stub function vpi_get_vlog_info called");
    //TODO
}

#[no_mangle]
pub extern "C" fn vpi_register_cb(cb_data_ptr: *mut t_cb_data) -> vpiHandle {
    //TESTING
    /*
    let cb_data = unsafe { *cb_data_ptr };
    dbg!(&cb_data);
    let vpi_handle = unsafe { cb_data.obj };
    let vpi_handle = unsafe { super::vpi_handle::VpiHandle::from_boxed_ffi_ptr(vpi_handle) };
    dbg!(&vpi_handle);
    std::mem::forget(vpi_handle);
    */

    //For fun, call $display
    //TESTING
    /*
    let test = crate::system_procedures::get_info("$display").unwrap();
    //unsafe { test.call_callback.unwrap()(c"TESTING123".as_ptr() as *mut i8) };
    unsafe { test.call_callback.unwrap()(test.user_data) };
    let test = crate::system_procedures::get_info("$finish").unwrap();
    unsafe { test.call_callback.unwrap()(c"TESTING123".as_ptr() as *mut i8) };
    */

    //todo!()

    //TESTING
    super::vpi_handle::VpiHandle::Testing.into_boxed_ffi_pointer()
}

#[no_mangle]
pub extern "C" fn vpi_handle(type_: PLI_INT32, refHandle: vpiHandle) -> vpiHandle {
    //TESTING
    //Interesting that we see $display reach for vpiSysTfCall, likely to get the arguments!
    dbg!(type_);
    dbg!(refHandle);
    todo!()
}

#[no_mangle]
pub extern "C" fn vpip_make_systf_system_defined() {
    println!("Stub function vpip_make_systf_system_defined() called, making this a no-op for now");
}

/* ------------------------------------------------------------------------------------------------
 * Tests
 * --------------------------------------------------------------------------------------------- */

//TODO

/* ------------------------------------------------------------------------------------------------
 * Benchmarks
 * --------------------------------------------------------------------------------------------- */

//TODO
